//He fet la sincronitzaci� amb el GIT

import java.util.Scanner;

public class Calculadora {

	static Scanner src = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		char opcio;
		int op1 = 0, op2 = 0;
		int res = 0;
		boolean dades = false;
		
		do {
			
			opcio = mostrarMenu();	//m�tode definit per nosaltres. Retorna l'opci� de men� escollida per l'usuari
			
			switch (opcio) {
                case 'o':
                case 'O': if (!dades) {
                			op1 = llegirDada();
                		  	op2 = llegirDada();
                		  	dades = true;
                		} else {
                			op1 = res;
                			op2 = llegirDada();
                		}
                    break;
                case '+': 
                		if (dades == true)
                			res = sumar(op1, op2);
                		else System.out.println("Cal llegir primer les dades");
                    break;
                case '-': if (dades == true)
                			res = restar(op1, op2);
                		  else System.out.println("Cal llegir primer les dades");
                    break;
                case '*':
                	if (dades == true) res = multiplicacar(op1, op2);
                		else System.out.println("Cal llegir primer les dades");
                    break;
                case '/': if (dades == true) res = dividir(op1, op2);
                		  else System.out.println("Cal llegir primer les dades");
                	 break;
                case 'v':
                case 'V': if (dades == true) visualitzar(res);
                		  else System.out.println("Cal llegir primer les dades");
                    break;
                case 'C':
                case 'c': dades = false;
                	break;
                case 's':
                case 'S':
                    System.out.print("Acabem.....");
                    break;
                default:
                    System.out.print("opci� erronia");
			}
			;
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");

	}

	
	public static int llegirDada() {
		// Llegeix un valor enter i el retorna
		int valor = 0;
		
		boolean correcte = false;
		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = src.nextInt();
				src.nextLine();
				correcte = true;
			}
			catch (Exception e) {
				System.out.println("Error, cal introduir un valor enter");
				src.nextLine();
			}
		} while (!correcte);
		
		return valor;
	}

	public static char mostrarMenu() {
		//Llegeix una opci� del men�, la valida i la retorna al main()
		char opcio;
		boolean correcte = false;
		
		do {
			System.out.println("\nCalculadora:\n");
			System.out.println("o.- Obtenir els valors");
			System.out.println("+.- Sumar");
			System.out.println("-.- Restar");
			System.out.println("*.- Multiplicar");
			System.out.println("/.- Dividir");
			System.out.println("v.- Visualitzar Operadors");
			System.out.println("C.- Reiniciar calculadora");
			System.out.println("S. Sortir");
			System.out.print("\n\nTria una opci�: ");
			
			opcio = src.nextLine().charAt(0);
			if (opcio == 'o' || opcio == 'O' || opcio == 's' || opcio == 'S' || opcio == '+' || opcio == '-' || opcio == '*' ||
				opcio == '/' || opcio == 'v' || opcio == 'V' || opcio == 'c' || opcio == 'C')
					correcte = true;
			else
				System.out.println("Error, opci� incorrecte. Torna-ho a provar");

		} while(!correcte);
		
		System.out.println("El m�tode retorna: " + opcio);
		
		return opcio;  //retorna el valor guardat en opcio a qui ho ha cridat
	}

	public static int sumar(int a, int b) { 
		int res;
		res = a + b;
		return res;
	}

	public static int restar(int a, int b) { 
		int res;
		res = a - b;
		return res;
	}

	public static int multiplicacar(int a, int b) { 
		int res;
		res = a * b;
		return res;
	}

	public static int dividir(int a, int b) { 
		char op;
		int res = -1; 
		
		if (b == 0) System.out.println("Error, no es pot dividir entre zero");
		else {
			do {
				System.out.println("M. " + a + " mod " + b);
				System.out.println("D  " + a + " / " + b);
				//op = llegirCar();
				
				op = src.nextLine().charAt(0);

				if (op == 'M' || op == 'm')
					res = a % b;
				else if (op == 'D' || op == 'd')
					res = a / b;
				else
					System.out.print("opci� incorrecte\n");
			} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');
		}

		return res;
	}
	
	public static void visualitzar(int res) { 
		System.out.println("\nEl resuktat de l''operacio �s " + res);
	}


	public static char llegirCar() 
	{
		char car;
		
		System.out.print("Introdueix un car�cter: ");
		car = src.nextLine().charAt(0);

		return car;
	}

}
